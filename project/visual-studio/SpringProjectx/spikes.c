#include "common.h"

static void touch(Entity* other);

void initSpikes(char* line) {

	Entity* e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));

	stage.entityTail->next = e;
	stage.entityTail = e;

	sscanf(line, "%*s %f %f", &e->x, &e->y);
	e->health = 1;
	e->texture = loadTexture("gfx/Spikes.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_SOLID;
}


static void touch(Entity* other) {

	if (self->health > 0 && other == player)
	{
		player->health = 0;
		printf("GAME OVER\n");
		printf("Press spacebar to try again");
		exit(1);
	}

}